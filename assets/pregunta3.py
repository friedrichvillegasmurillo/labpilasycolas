from io import open
from collections import deque
#3. Implementar una aplicación que lea un archivo de texto plano y extraiga palabra por palabra,
#insertando en una cola sólo las palabras que terminan en consonante. Posteriormente debe permitir
#leer una palabra y buscar si la misma existe en la lista, determinando su posición.
pertlifo = (
    "b", 
    "d", 
    "f", 
    "g", 
    "h", 
    "j", 
    "k", 
    "l", 
    "m", 
    "n", 
    "ñ", 
    "p", 
    "q", 
    "r", 
    "s", 
    "t", 
    "v", 
    "x", 
    "y", 
    "z")
#print(pertlifo)

def textwriter():
    file = open("archivo_extrae3.txt", "w")
    writer = input("Por favor introduzca una oración: ") 
    file.write(writer)
    file.close()

def textreader():
    filereader = open("archivo_extrae3.txt", "r")
    reader = filereader.read()
    #print (reader)

def consonante():
    #leo el archivo escrito
    filereader = open("archivo_extrae3.txt", "r")
    reader = filereader.read()
    readerspliter = reader.split()

    print(readerspliter)
    #obtengo la longitud del texto como lista
    lentaker = len(readerspliter)
    #obtengo la longitud de las consonantes como lista
    lentaker2 = len(pertlifo)
    #variable para obtener el ultimo caracter de una palabra
    spliter = ""
    lenplacer=[]

    for i in range(lentaker):
        spliter =readerspliter[i]
        #print(spliter)
        for j in range(lentaker2):
            if spliter[-1] == pertlifo[j]:
                lenplacer.append(readerspliter[i])
    #print(lenplacer)

    file = open("archivo_inserta3.txt", "w")
    lentaker = len(lenplacer)
    for i in range(lentaker):
        file.write('%s'%lenplacer[i] + "\n")
    file.close()
    filereader = open("archivo_inserta3.txt", "r")
    reader = filereader.read()
    print (reader)

textwriter()
textreader()
consonante()