import turtle 
import time
import random 

# pospone
t_pospone = 0.2

#window be like this
from turtle import * 

tracer(0)
setup(600,600)
pencolor("black")
bgcolor("white")

title("Snake")
wcor  = turtle.Turtle()
wcor.hideturtle()
penup()
#cuadrado limitrofe

goto (-300,-300)
down()
forward(600)
left(90)
forward(600)
left(90)
forward(600)
left(90)
forward(600)
left(90)
#cuadrantes horizontales

goto(-300,100)
down()
forward(600)
up()
goto(-300,-100)
down()
forward(600)
up()
#cuadrantes verticales

goto(-100,300)
setheading(-90)
down()
forward(600)
up()
goto(100,300)
down()
forward(600)
up()
tracer(0)
hideturtle()
#head animation
head = turtle.Turtle()
head.speed(0)
head.shape("square")
head.color("black")
head.penup()
head.goto(0,0)
head.direction = ["","","","","","","","",""]

#random 
ran_x = random.randint(-270,270)
ran_y = random.randint(-270,240)
#point animation
eat_point = turtle.Turtle()
eat_point.shapesize(1.5)
eat_point.speed(0)
eat_point.shape("circle")
eat_point.color("black")
eat_point.penup()
eat_point.goto(ran_x, ran_y)
#cuerpo
body = list([])
body_count= 0	
#    body_count.hitdeturtle()
#	body_count.reset()
#	body_count.clear()

#write in screen 
writer = turtle.Turtle()
writer.speed(0)
writer.shape("circle")
writer.color("black")
writer.penup()
writer.hideturtle()
writer.goto(20,270)
writer.write("Score: 0  High Score: 0", align = "left", font = ("Arial" , 5, "normal"))

		
#variables
score = 0
high_score = 0
rango = None
rango_aux= rango
arriba = "up"
abajo = "down"
izquierda = "left"
derecha = "right"

#functions
def mov():
		
	if head.direction == ["","up","","","","","","",""]:
		y = head.ycor()
		head.sety( y + 20)
		
	if head.direction == ["","","","","","","","down",""]:
		y = head.ycor()
		head.sety( y - 20)
	
	if head.direction == ["","","","left","","","","",""]:
		x = head.xcor()
		head.setx( x - 20)
	
	if head.direction == ["","","","","","right","","",""]:
		x = head.xcor()
		head.setx( x + 20)
		
penup()

#def cuadrante
def mov_tap(x, y):
	global arriba
	global abajo
	global izquierda
	global derecha
	global mov

	columna = (x + 300) // 200
	fila = (-y +300) // 200
	rango = columna +fila*3
	rango = int(rango)

	#excepcion de rango
	
	if rango < 0 and head.direction !=  ["","","","","","","","down",""]:
		head.direction = ["","","","","","","","",""]
		head.direction[1] = arriba
	if rango > 8 and head.direction != ["","up","","","","","","",""]:
		head.direction = ["","","","","","","","",""]
		head.direction[7] = abajo		
	if rango == 4 and head.direction !=  ["","","","","","","","down",""]:
		head.direction = ["","","","","","","","",""]
		head.direction[1] = arriba
	# arriba der	
	if rango == 2 and head.direction != ["","","","left","","","","",""]:
		head.direction = ["","","","","","","","",""]
		head.direction[5] = derecha
	# arriba izq
	if rango == 0 and head.direction != ["","","","","","right","","",""]:
		head.direction = ["","","","","","","","",""]
		head.direction[3] = izquierda
	# abajo der		
	if rango == 8 and head.direction != ["","","","left","","","","",""]:
		head.direction = ["","","","","","","","",""]
		head.direction[5] = derecha
		# abajo izq
	if rango == 6 and head.direction != ["","","","","","right","","",""]:
		head.direction = ["","","","","","","","",""]
		head.direction[3] = izquierda

 #movimiento
	if rango == 1 and head.direction !=  ["","","","","","","","down",""]:
		head.direction = ["","","","","","","","",""]
		head.direction[rango] = arriba
	elif rango == 7 and head.direction != ["","up","","","","","","",""]:
		head.direction = ["","","","","","","","",""]
		head.direction[rango] = abajo
	elif rango == 3 and head.direction != ["","","","","","right","","",""]:
		head.direction = ["","","","","","","","",""]
		head.direction[rango] = izquierda
	elif rango == 5 and head.direction != ["","","","left","","","","",""]:
		head.direction = ["","","","","","","","",""]
		head.direction[rango] = derecha
			
onscreenclick(mov_tap)
#bucle
while True:
	update()
	if head.xcor() > 285 or head.xcor() < -285 or head.ycor() > 250 or head.ycor() < -285:
		time.sleep(1)
		head.direction = ["","","","","","","","",""]
		head.hideturtle()
		time.sleep(1)
		head.showturtle()	
		head.direction = ["","","","","","","","",""]
		head.goto(0,0)
		x_r = random.randint(-270, 270)
		y_r = random.randint(-270, 240)
		eat_point.goto(x_r, y_r) 
		for ins in body:
			ins.hideturtle()
		
		score = 0
		body_count = 0
		body.clear()
			
	
	if eat_point.distance(head) < 17:
		x_r = random.randint(-270, 270)
		y_r = random.randint(-270, 240)
		eat_point.goto(x_r, y_r)
		#global body_count

		aux= turtle.Turtle()
		aux.speed(0)
		aux.shape("square")
		aux.color("green")
		aux.penup()
		body.insert(body_count, aux)
		body_count = body_count + 1
		
		score += 10
	if score > high_score:
		high_score = score
	writer.clear()
	writer.write("Score: {}  High Score: {}".format(score, high_score), align = "left", font = ("Arial" , 5, "normal"))
		
		
	body_list = len(body)
	for insert in range(body_list -1, 0, -1):
		x = body[insert -1].xcor()
		y = body[insert -1].ycor()
		body[insert].goto(x, y)
	if body_list > 0:
		x = head.xcor()
		y = head.ycor()
		body[0].goto(x, y)
	
	mov()
	
	for ins in body:
		if ins.distance(head) < 10:
			time.sleep(1)
			head.goto(0,0)
			head.direction = ["","","","","","","","",""]
			
			for ins in body:
				ins.hideturtle()
			score = 0
			body_count = 0
			body.clear()		
		
	
	time.sleep(t_pospone)
	
mainloop()