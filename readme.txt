ESTRUCTURA DE DATOS
LABORATORIO DE PILAS, COLAS Y LISTAS

1. Implementar una aplicación que lea un archivo de texto plano y extraiga palabra por palabra
insertando en una cola sólo las pablas con más de 5 caracteres. Posteriormente escribir un segundo
archivo de textos extrayendo las palabras desde la cola.

Nota: La respuesta a la pregunta Nº 1 se almacena dentro del archivo assets con el nombre correspondiente 
de "pregunta1.py"
2. Implementar una aplicación que lea un archivo de texto plano y extraiga palabra por palabra
insertando en una pila sólo las pablas con menos de 6 caracteres. Posteriormente escribir un segundo
archivo de textos extrayendo las palabras desde la pila.

Nota: La respuesta a la pregunta Nº 2 se almacena dentro del archivo assets con el nombre correspondiente 
de "pregunta2.py"
3. Implementar una aplicación que lea un archivo de texto plano y extraiga palabra por palabra,
insertando en una cola sólo las palabras que terminan en consonante. Posteriormente debe permitir
leer una palabra y buscar si la misma existe en la lista, determinando su posición.

Nota: La respuesta a la pregunta Nº 3 se almacena dentro del archivo assets con el nombre correspondiente 
de "pregunta3.py"


Los archivos de texto luego de ser ejecutados llevan numerados a que pregunta corresponden fuera del archivo assets

///// Se puede jugar a la vivorita si quiere "vivo.py" :)